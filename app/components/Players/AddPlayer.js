import React from 'react';

require('./addPlayer.scss');

class AddPlayer extends React.Component {
    handleSubmit() {
        const newPlayer = {
            name: this.refs.playerName.getDOMNode().value,
            email: this.refs.playerEmail.getDOMNode().value,
            reply: true
        };
        this.props.add(newPlayer);
    }
    render() {
        return (
            <div className="col-md-4 col-md-offset-2">

                <div className="introText">
                    <h3>Sign up if you haven't played before...</h3>
                </div>

                <form className="signUpForm">

                    <div className="form-group">
                        <label>NAME</label>
                        <input
                            type="name"
                            className="form-control"
                            placeholder="NAME"
                            ref="playerName"/>
                    </div>

                    <div className="form-group">
                        <label>EMAIL ADDRESS </label>
                        <input
                            type="email"
                            className="form-control"
                            placeholder="EMAIL"
                            ref="playerEmail"/>
                    </div>

                    <button
                        type="submit"
                        className="btn btn-primary"
                        onClick={this.handleSubmit.bind(this)}>SUBMIT</button>

                </form>
            </div>
        );
    }
}

// AddPlayer.propTypes = {
//     name: React.PropTypes.string.isRequired,
//     email: React.PropTypes.string.isRequired
// };

export default AddPlayer;
