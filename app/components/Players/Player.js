import React from 'react';
import NotesList from './PlayersList';
import AddNote from './AddPlayer';

require('./player.scss');

const Players = ({name, email, addPlayer}) => {
    return (
        <div>
            <AddNote name={name} addPlayer={addPlayer} />
            <PlayersList players={players} />
        </div>
    );
};

Notes.propTypes = {
    name: React.PropTypes.string.isRequired,
    email: React.PropTypes.string.isRequired,
    addNote: React.PropTypes.func.isRequired
};

export default Players;
