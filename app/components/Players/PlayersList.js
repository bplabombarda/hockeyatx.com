import React from 'react';

require('./playersList.scss');

class PlayersList extends React.Component{
    render(){

        const listItems = this.props.players.map((item, index) => {
            return (
                <li key={item.key} className="player">
                    <span className={"rsvpButton " + (item.reply === true ? "replyYes" : "")} onClick={this.props.reply.bind(null, index)}>
                        {item.reply === true ? "YES" : "NO"}
                    </span>
                    <span className="playerName">
                        {item.name}
                    </span>
                    <span className="removePlayer" onClick={this.props.remove.bind(null, index)}>
                        <sup><i className="fa fa-times-circle-o"></i></sup>
                    </span>
                </li>
            );
        });
        return (
            <div className="col-md-6">
                <div className="introText">
                    <h3>...or find your name here if you have.</h3>
                </div>
                <ul className="playerList">
                    {listItems}
                </ul>
            </div>
        );
    }
}

// PlayersList.propTypes = {
//     name: React.PropTypes.string.isRequired,
//     email: React.PropTypes.string.isRequired
// };

export default PlayersList;
