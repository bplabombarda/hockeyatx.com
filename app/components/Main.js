import React from 'react';
import Rebase from 're-base';
import AddPlayer from './Players/AddPlayer';
import PlayersList from './Players/PlayersList';

var base = Rebase.createClass('https://eager-tangerine-2263.firebaseio.com/');

class Main extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            playersList: [],
            loading: true
        };
    }
    componentDidMount(){
        this.ref = base.syncState('playersList', {
            context: this,
            state: 'playersList',
            asArray: true,
            then() {
                this.setState({loading: false});
            }
        });
    }
    componentWillUnmount(){
        base.removeBinding(this.ref);
    }
    handleAddPlayer(newPlayer){
        this.setState({
            playersList: this.state.playersList.concat([newPlayer])
        });
    }
    handlePlayerReply(index) {
        const newPlayersList = this.state.playersList;

        newPlayersList[index].reply = !newPlayersList[index].reply;

        this.setState({
            playersList: newPlayersList
        });
    }
    handleRemovePlayer(index){
        const newPlayersList = this.state.playersList;
        newPlayersList.splice(index, 1);
        this.setState({
            playersList: newPlayersList
        });
    }
    render(){
        return (
            <div className="container">
                <div className="row">
                    <AddPlayer add={this.handleAddPlayer.bind(this)}/>
                    {this.state.loading === true ? <h3> LOADING... </h3> : <PlayersList players={this.state.playersList} reply={this.handlePlayerReply.bind(this)} remove={this.handleRemovePlayer.bind(this)}/>}
                </div>
            </div>
        );
    }
}

export default Main;
